package com.example.demo.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.example.demo.modules.sys.entity.Emp;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


@Mapper
public interface EmptDao extends BaseMapper<Emp> {

    void insertEmp(Emp emp);
    List<Emp> selectEmpAll();

    /**
     *
     * @param
     * @return
     */
    List<Emp> selelctLimit(Map<String,Object> map);
}
