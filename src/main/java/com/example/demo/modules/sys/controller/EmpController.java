package com.example.demo.modules.sys.controller;

import com.example.demo.commons.utils.R;
import com.example.demo.modules.sys.dao.EmptDao;
import com.example.demo.modules.sys.entity.Emp;
import com.example.demo.commons.utils.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class EmpController {
    @Autowired
    EmptDao emptDao;

    @RequestMapping("/insertEmp")
    @ResponseBody
    public String insertEmp(@RequestBody Emp emp) {
        emptDao.insertEmp(emp);
        return "成功";
    }

    @RequestMapping("/selectEmpAll")
    @ResponseBody
    public List<Emp> selectEmpAll() {
       List<Emp> emp = emptDao.selectEmpAll();
        return emp;
    }

    /**
     *
     * @param page
     * @return
     */
    @RequestMapping("/selelctLimit")
    @ResponseBody
    public R selelctLimit(Page page) {
        Map<String,Object> map = new HashMap<>();
        map.put("limit0",page.getLimit0());
        map.put("limit1",page.getLimit1());
       List<Emp> empList =  emptDao.selelctLimit(map);
        return R.ok().put("empList",empList);
    }
}
