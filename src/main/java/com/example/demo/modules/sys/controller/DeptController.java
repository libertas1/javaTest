package com.example.demo.modules.sys.controller;

import com.example.demo.modules.sys.dao.DeptDao;
import com.example.demo.modules.sys.entity.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class DeptController {
    @Autowired
    DeptDao deptDao;
    @RequestMapping("/insertDept")
    @ResponseBody
    public String insertDept(@RequestBody Dept dept) {
        deptDao.insertDept(dept);
        return "成功";
    }

    @RequestMapping("/dropDept")
    @ResponseBody
    public String dropDept(@RequestBody Dept dept) {
        deptDao.dropDept(dept);
        return "成功";
    }

    @RequestMapping("/selectAll")
    @ResponseBody
    public List<Dept> selectAll() {
       List<Dept> dept= deptDao.selectAll();
        return  dept;
    }
}
