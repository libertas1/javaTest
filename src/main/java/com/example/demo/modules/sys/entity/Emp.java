package com.example.demo.modules.sys.entity;



public class Emp {
    private int empId;
    private String empName;
    private String gender;
    private String email;
    private int dId;
    private Dept dept;

    public int getEmpId(){
    return empId;
}
    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getdId() {
        return dId;
    }

    public void setdId(int dId) {
        this.dId = dId;
    }

    public String getEmpName(){
        return empName;
    }

    public String getGender(){
        return gender;
    }

    public String getEmail(){
        return email;
    }

    public void setEmpName(String empName){
        this.empName=empName;
    }

    public void setGender(String gender){
        this.gender=gender;
    }

    public void setEmail(String email){
        this.email=email;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }
}
