package com.example.demo.modules.sys.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.example.demo.modules.sys.entity.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


@Mapper
public interface DeptDao extends BaseMapper<Dept> {
    void insertDept(Dept dept);
    void dropDept(Dept dept);
    List<Dept> selectAll();
}
