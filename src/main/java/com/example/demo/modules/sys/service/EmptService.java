package com.example.demo.modules.sys.service;

import com.example.demo.modules.sys.dao.EmptDao;
import com.example.demo.modules.sys.entity.Emp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class EmptService {
    @Autowired
    EmptDao emptDao;

    public List<Emp> selelctLimit(Map<String,Object> map){
        return emptDao.selelctLimit(map);
    }

}
